import logging
import re
import sys

class Logger:
  def __init__(self, name, level):
    self.logger = logging.getLogger(name)
    formatter = logging.Formatter('%(asctime)s : [%(levelname)s] %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    fh = logging.FileHandler('prettyAlerts.log')
    fh.setFormatter(formatter)
    self.logger.addHandler(ch)
    self.logger.addHandler(fh)
    if bool(re.search('debug', level, re.IGNORECASE)):
      self.logger.setLevel(logging.DEBUG)
    elif bool(re.search('info', level, re.IGNORECASE)):
      self.logger.setLevel(logging.INFO)
    elif bool(re.search('warning', level, re.IGNORECASE)):
      self.logger.setLevel(logging.WARNING)
    elif bool(re.search('error', level, re.IGNORECASE)):
      self.logger.setLevel(logging.ERROR)
    elif bool(re.search('critical', level, re.IGNORECASE)):
      self.logger.setLevel(logging.CRITICAL)
    else:
      self.logger.setLevel(logging.DEBUG)
  
  def debug(self, *args):
    self.logger.debug(*args)
  def info(self, *args):
    self.logger.info(*args)
  def warn(self, *args):
    self.logger.warning(*args)
  def err(self, *args):
    self.logger.error(*args)
  def crit(self, *args):
    self.logger.critical(*args)
    sys.exit(1)

if __name__ == '__main__':
  logger = Logger(__name__, 'debug')
  logger.debug('test')
