# Description
Image based alert plugin for nagios and compatible monitoring systems. Supports slack, mattermost, discord, synology chat, etc...

![image](https://i.imgur.com/Ftt4gOc.png)

Now using [solorized](https://ethanschoonover.com/solarized/) color scheme!

# Installation
* `git clone https://gitlab.com/Jedimaster0/prettyAlerts.git`
* `cd prettyAlerts`
* `pip install .`
* `sudo yum install chromedriver`
* `sudo yum install chromium`

# Usage
```
usage: prettyAlerts.py [-h] --nagioshost NAGIOSHOST --serviceorhost                                                                                                                                          │·······
                       {host,service} --hostalias HOSTALIAS --notificationtype                                                                                                                               │·······
                       NOTIFICATIONTYPE [--servicestate SERVICESTATE]                                                                                                                                        │·······
                       [--servicedesc SERVICEDESC]                                                                                                                                                           │·······
                       [--serviceoutput SERVICEOUTPUT]                                                                                                                                                       │·······
                       [--servicenotes SERVICENOTES] [--hoststate HOSTSTATE]                                                                                                                                 │·······
                       [--hostoutput HOSTOUTPUT] [--hostnotes HOSTNOTES]                                                                                                                                     │·······
                       [--webhookusername WEBHOOKUSERNAME]                                                                                                                                                   │·······
                       [--webhookchannel WEBHOOKCHANNEL] --webhookurl                                                                                                                                        │·······
                       WEBHOOKURL [--imgurl IMGURL]                                                                                                                                                          │·······
                       [--platform {slack,mattermost,synchat,discord}]                                                                                                                                       │·······
                       [--loglevel {debug,info,warning,error,critical}]                                                                                                                                      │·······
                       [--port PORT] [--address ADDRESS]                                                                                                                                                     │·······
                                                                                                                                                                                                             │·······
optional arguments:                                                                                                                                                                                          │·······
  -h, --help            show this help message and exit                                                                                                                                                      │·······
  --nagioshost NAGIOSHOST                                                                                                                                                                                    │·······
                        Nagios server host name                                                                                                                                                              │·······
  --serviceorhost {host,service}                                                                                                                                                                             │·······
                        Alert type                                                                                                                                                                           │·······
  --hostalias HOSTALIAS                                                                                                                                                                                      │·······
                        $HOSTNAME$                                                                                                                                                                           │·······
  --notificationtype NOTIFICATIONTYPE                                                                                                                                                                        │·······
                        $NOTIFICATIONTYPE$                                                                                                                                                                   │·······
  --servicestate SERVICESTATE                                                                                                                                                                                │·······
                        $SERVICESTATE$                                                                                                                                                                       │·······
  --servicedesc SERVICEDESC                                                                                                                                                                                  │·······
                        $SERVICEDESC$                                                                                                                                                                        │·······
  --serviceoutput SERVICEOUTPUT                                                                                                                                                                              │·······
                        $SERVICEOUTPUT$                                                                                                                                                                      │·······
  --servicenotes SERVICENOTES                                                                                                                                                                                │·······
                        $SERVICENOTES$                                                                                                                                                                       │·······
  --hoststate HOSTSTATE                                                                                                                                                                                      │·······
                        $HOSTSTATE$                                                                                                                                                                          │·······
  --hostoutput HOSTOUTPUT                                                                                                                                                                                    │·······
                        $HOSTOUTPUT$                                                                                                                                                                         │·······
  --hostnotes HOSTNOTES                                                                                                                                                                                      │·······
                        $HOSTNOTES$                                                                                                                                                                          │·······
  --webhookusername WEBHOOKUSERNAME                                                                                                                                                                          │·······
                        Webhook username                                                                                                                                                                     │·······
  --webhookchannel WEBHOOKCHANNEL                                                                                                                                                                            │·······
                        Webhook channel. DEFAULT: #alerts                                                                                                                                                    │·······
  --webhookurl WEBHOOKURL                                                                                                                                                                                    │·······
                        Webhook URL                                                                                                                                                                          │·······
  --imgurl IMGURL       Image icon URL                                                                                                                                                                       │·······
  --platform {slack,mattermost,synchat,discord}                                                                                                                                                              │·······
                        Chat platform type                                                                                                                                                                   │·······
  --loglevel {debug,info,warning,error,critical}                                                                                                                                                             │·······
                        Log level                                                                                                                                                                            │·······
  --port PORT           Webserver port 8080                                                                                                                                                                  │·······
  --address ADDRESS     Webserver bind address. DEFAULT: localhost
```